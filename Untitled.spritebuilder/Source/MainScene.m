#import "MainScene.h"
#import "cocos2d.h"
#import "CCPackageManager.h"
#import "CCPackageManagerDelegate.h"

@interface MainScene () <CCPackageManagerDelegate>

@end

@implementation MainScene

- (void) action {
    //CCScene *scene = [CCBReader loadAsScene:@"GirlShow"];
    //[[CCDirector sharedDirector] replaceScene:scene];
    [CCPackageManager sharedManager].baseURL = [NSURL URLWithString:@"http://upfile.vn/download/member/FaZCZQBmvgi-/qsGiF5ZmNQBm/Z_cjfm8MX7rr/fp8odJrod_Fw/6c9f9f0e499015305/1415263065/1cd9c8cc49f0e8e12c7b8c9036cfd25e019ba4d0cdd258581"];
    // 2.
    [[CCPackageManager sharedManager] downloadPackageWithName:@"Girl"
     // 3.
                                          enableAfterDownload:YES];
    // 4.
    [CCPackageManager sharedManager].delegate = self;
}

#pragma mark - PackageManagerDelegate method
/*- (NSString *)customFolderName:(CCPackage *)package packageContents:(NSArray *)packageContents {
 return @"Girl-iOS-phonehd.sbpack";
 }*/

- (void)packageInstallationFinished:(CCPackage *)package{
    // Can use package from here
    CCScene *scene = [CCBReader loadAsScene:@"GirlShow"];
    [[CCDirector sharedDirector] replaceScene:scene];
    
}

- (void)packageInstallationFailed:(CCPackage *)package error:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Package" message:@"Install Package Fail!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}


- (void)packageDownloadFinished:(CCPackage *)package{
    
}

- (void)packageDownloadFailed:(CCPackage *)package error:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Package" message:@"Download Package Fail!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (void)packageUnzippingFinished:(CCPackage *)package {
    
}
- (void)packageUnzippingFailed:(CCPackage *)package error:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Package" message:@"Unzipping Package Fail!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (void)packageDownloadProgress:(CCPackage *)package downloadedBytes:(NSUInteger)downloadedBytes totalBytes:(NSUInteger)totalBytes{
    float percent = (float)downloadedBytes/ totalBytes * 100;
    dispatch_async(dispatch_get_main_queue(), ^{
        _lblDownload.string = [NSString stringWithFormat:@"%.0f", percent];
    });

    
    NSLog(@"%f", percent);
}

- (void)packageUnzippingProgress:(CCPackage *)package unzippedBytes:(NSUInteger)unzippedBytes totalBytes:(NSUInteger)totalBytes {
    float percent = (float)unzippedBytes/ totalBytes *100;
    dispatch_async(dispatch_get_main_queue(), ^{
      _lblUnzip.string = [NSString stringWithFormat:@"%.0f", percent];
    });
    
    
    NSLog(@"%f", percent);
}

- (void) update:(CCTime)delta {
    
}
@end
