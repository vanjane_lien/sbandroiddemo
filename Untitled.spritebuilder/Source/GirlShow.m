//
//  GirlShow.m
//  Untitled
//
//  Created by Thuy Lien Nguyen on 11/6/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "GirlShow.h"

@implementation GirlShow

- (void)didLoadFromCCB {
    
    [_girlPhoto setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"girl_1.png"]];
    
    [self animateGirl];
    
}

-(void)animateGirl
{
    NSMutableArray *animFrames = [[NSMutableArray alloc] init];
    
    for(int i = 1; i < 11; i++)
    {
        CCSpriteFrame *frame = [CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"girl_%d.png",i]];
        [animFrames addObject:frame];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames: animFrames delay:2.0f];
    
    //Repeating the sprite animation
    CCActionAnimate *animationAction = [CCActionAnimate actionWithAnimation:animation];
    CCActionRepeatForever *repeatingAnimation = [CCActionRepeatForever actionWithAction:animationAction];
    [_girlPhoto runAction:repeatingAnimation];
    //[_girlPhoto runAction:[CCActionRepeatForever actionWithAction:[CCActionAnimate actionWithAnimation:animation]]];
   
    
}
@end
